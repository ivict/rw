package com.usalko.ringowatcher.fragments;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.SwitchPreference;
import android.text.TextUtils;
import android.view.MenuItem;

import com.usalko.ringowatcher.R;
import com.usalko.ringowatcher.RingoWatcherActivity;
import com.usalko.ringowatcher.Settings;
import com.usalko.ringowatcher.settings.EditFolderNamePreference;
import com.usalko.ringowatcher.settings.HashPasswordEditTextPreference;
import com.usalko.ringowatcher.settings.SettingsActivity;

/**
 * This fragment shows general preferences only. It is used when the
 * activity is showing a two-pane settings UI.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class GeneralPreferenceFragment extends PreferenceFragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref_general);
        setHasOptionsMenu(true);

        //@see http://stackoverflow.com/questions/22275517/how-to-set-default-value-for-preferences-in-preferencefragment-dynamically-not
        EditFolderNamePreference preference = (EditFolderNamePreference) getPreferenceManager().findPreference(Settings.FOLDER_FOR_AUTOPLAY);
        if (preference != null && getActivity() != null && TextUtils.isEmpty(preference.getText())) {
            String folderForAutoplay = ((RingoWatcherActivity) getActivity()).getApplicationContext().getSettings().getFolderForAutoplay().getPath();
            //preference.setDefaultValue(defaultFolderForAutoplay);
            preference.setText(folderForAutoplay);
        }
        SwitchPreference autoplayOnReboot = (SwitchPreference) getPreferenceManager().findPreference(Settings.ENABLE_AUTOPLAY_ON_REBOOT);
        if (autoplayOnReboot != null) {
            autoplayOnReboot.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    RingoWatcherActivity activity = (RingoWatcherActivity) getActivity();
                    if (activity != null) {
                        activity.getApplicationContext().getSettings().setEnableAutoplayOnReboot((Boolean) newValue);
                    }
                    return true;//If false then no changed
                }
            });
        }

        HashPasswordEditTextPreference passwordHash = (HashPasswordEditTextPreference) getPreferenceManager().findPreference(Settings.PASSWORD_HASH);
        if (((RingoWatcherActivity) getActivity()).getApplicationContext().getSettings().isRegistered()) {
            getPreferenceScreen().removePreference(passwordHash);
        } else {
            passwordHash.setAccepted(true);
        }


        // Bind the summaries of EditText/List/Dialog/Ringtone preferences
        // to their values. When their values change, their summaries are
        // updated to reflect the new value, per the Android Design
        // guidelines.
        SettingsActivity.bindPreferenceSummaryToValue(findPreference(Settings.FOLDER_FOR_AUTOPLAY));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            //startActivity(new Intent(getActivity(), SettingsActivity.class));
            //REDIRECT FOR INITIAL VERSION OF APP
            getActivity().onOptionsItemSelected(item);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
