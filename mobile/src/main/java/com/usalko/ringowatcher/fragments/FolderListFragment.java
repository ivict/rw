/*
 * Copyright (C) 2013 Paul Burke
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.usalko.ringowatcher.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;

import com.ipaulpro.afilechooser.FileListAdapter;
import com.ipaulpro.afilechooser.FileLoader;
import com.usalko.ringowatcher.R;
import com.usalko.ringowatcher.activities.FolderChooserActivity;
import com.usalko.tools.Tagger;

import org.icelog.clog.Clog;

import java.io.File;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Fragment that displays a list of Files in a given path.
 * 
 * @version 2013-12-11
 * @author paulburke (ipaulpro)
 */
public class FolderListFragment extends ListFragment implements
        LoaderManager.LoaderCallbacks<List<File>> {

    private static final String TAG = Tagger.tag(FolderListFragment.class);
    private boolean loaded;
    private Spinner spinner;

    /**
     * Interface to listen for events.
     */
    public interface Callbacks {
        /**
         * Called when a file is selected from the list.
         *
         * @param file The file selected
         */
        void onFileSelected(File file);
        void onFolderSelected(File file);
    }

    private static final int LOADER_ID = 0;
    private static final int STORAGES_LOADER_ID = 1;

    private FileListAdapter adapter;
    private FileListAdapter storagesAdapter;
    private String path;

    private Callbacks mListener;

    /**
     * Create a new instance with the given file path.
     *
     * @param path The absolute path of the file (directory) to display.
     * @return A new Fragment with the given file path.
     */
    public static FolderListFragment newInstance(String path) {
        FolderListFragment fragment = new FolderListFragment();
        Bundle args = new Bundle();
        args.putString(FolderChooserActivity.PATH, path);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mListener = (Callbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement FolderListFragment.Callbacks");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        adapter = new FileListAdapter(getActivity());
        path = getArguments() != null ? getArguments().getString(
                FolderChooserActivity.PATH) : Environment
                .getExternalStorageDirectory().getAbsolutePath();
        storagesAdapter = new FileListAdapter(getActivity());
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        setEmptyText(getString(R.string.empty_directory));
        setListAdapter(adapter);
        setListShown(false);

        getLoaderManager().initLoader(LOADER_ID, null, this);
        getLoaderManager().initLoader(STORAGES_LOADER_ID, null, this);

        View rootView = getView();
        if (rootView != null) {
            View chooseFolderButton = rootView.findViewById(R.id.file_list_fragment_$choose_this_folder$_button);
            chooseFolderButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    File file = new File(path);
                    if (!file.isDirectory()) {
                        mListener.onFolderSelected(file.getParentFile());
                    } else {
                        mListener.onFolderSelected(file);
                    }
                }
            });

            spinner = (Spinner) rootView.findViewById(R.id.file_list_fragment_$storage$_spinner);
            // Apply the adapter to the spinner
            spinner.setAdapter(storagesAdapter);

            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (!isLoaded()) {
                        return;
                    }
                    chooseFile(position, storagesAdapter);
                    ((FileLoader) getLoaderManager().<List<File>>getLoader(LOADER_ID)).setPath(path);
                    getLoaderManager().getLoader(LOADER_ID).onContentChanged();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    //Nothing do
                }
            });

        }

        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        FileListAdapter adapter = (FileListAdapter) l.getAdapter();
        if (adapter != null) {
            chooseFile(position, adapter);
        }
    }

    private void chooseFile(int position, FileListAdapter adapter) {
        File file = (File) adapter.getItem(position);
        path = file.getAbsolutePath();
        mListener.onFileSelected(file);
    }

    @Override
    public Loader<List<File>> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case LOADER_ID:
                return new FileLoader(getActivity(), path);
            case STORAGES_LOADER_ID:
                return new FileLoader(getActivity(), "/storage");
            default:
                throw new IllegalStateException("Unknown loader id: " + id);
        }
    }

    @Override
    public void onLoadFinished(Loader<List<File>> loader, List<File> data) {
        if (data == null) {
            Clog.e(TAG, "on-load-finished [data is null]");
            return;
        }
        switch (loader.getId()) {
            case LOADER_ID:
                adapter.setListItems(data);
                break;
            case STORAGES_LOADER_ID:
                setLoaded(false);
                storagesAdapter.setListItems(data);
                selectSpinner(data);
                getListView().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        setLoaded(true);
                    }
                }, TimeUnit.SECONDS.toMillis(2));//Wait for set item
                break;
            default:
                throw new IllegalStateException("Unknown loader id: " + loader.getId());
        }

        if (isResumed()) {
            setListShown(true);
        } else {
            setListShownNoAnimation(true);
        }
    }

    private void selectSpinner(List<File> data) {
        if (path == null) {
            Clog.e(TAG, "select-spinner [path is null]");
        }
        if (spinner == null) {
            Clog.e(TAG, "select-spinner [spinner is null]");
        }
        int position = -1;
        for (int i = 0; i < data.size(); i++) {
            File f = data.get(i);
            if (path.startsWith(f.getAbsolutePath())) {
                position = i;
                break;
            }
        }
        if (position < 0) {
            Clog.d(TAG, "select-spinner [not found path: " + path + ", in data list]");
            return;
        }
        spinner.setSelection(position);
    }

    @Override
    public void onLoaderReset(Loader<List<File>> loader) {
        adapter.clear();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.file_list_fragment, container, false);
        View superview = super.onCreateView(inflater, (ViewGroup) rootView, savedInstanceState);
        LinearLayout listContainer = (LinearLayout) rootView.findViewById(R.id.file_list_fragment_$list_container$_linear_layout);
        listContainer.addView(superview);
        return rootView;
    }

    public boolean isLoaded() {
        return loaded;
    }

    public void setLoaded(boolean loaded) {
        this.loaded = loaded;
    }

}
