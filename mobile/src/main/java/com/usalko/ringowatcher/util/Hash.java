package com.usalko.ringowatcher.util;

import com.usalko.tools.Tagger;

import org.icelog.clog.Clog;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Copyright (C) usalko.com - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ivan Usalko <ivict@usalko.com>, Май 2016
 */
public class Hash {

    private static final String TAG = Tagger.tag(Hash.class);
    public static final String DEFAULT_PASSWORD_HASH = "fce2fb86d67a05a2cec002372737304a18ff58d97587c058ecf2a99f2fb2ff99";
    //public static final String DEFAULT_PASSWORD = uTiNg0oozaeb

    private Hash() {
    }

    private static String bytesToHexString(byte[] bytes) {
        // http://stackoverflow.com/questions/332079
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(0xFF & bytes[i]);
            if (hex.length() == 1) {
                sb.append('0');
            }
            sb.append(hex);
        }
        return sb.toString();
    }

    // generate a hash SHA-256
    public static String sha256(String password) {
        if (password == null) {
            Clog.d(TAG, "error [password is null]");
            return null;
        }
        if (password.isEmpty() || password.trim().isEmpty()) {
            Clog.d(TAG, "error [password is empty or blank]");
            return null;
        }
        MessageDigest digest = null;
        String hash;
        try {
            digest = MessageDigest.getInstance("SHA-256");
            digest.update(password.getBytes());

            hash = bytesToHexString(digest.digest());

            Clog.d(TAG, "result is " + hash);
            return hash;
        }
        catch(NoSuchAlgorithmException e1) {
            Clog.e(TAG, e1);
        }
        return null;
    }

    // generate a hash SHA-1
    public static String sha1(String password) {
        MessageDigest digest = null;
        String hash;
        try {
            digest = MessageDigest.getInstance("SHA-1");
            digest.update(password.getBytes());

            hash = bytesToHexString(digest.digest());

            Clog.d(TAG, "result is " + hash);
            return hash;
        }
        catch(NoSuchAlgorithmException e1) {
            Clog.e(TAG, e1);
        }
        return null;
    }

}
