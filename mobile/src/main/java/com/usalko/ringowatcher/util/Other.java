package com.usalko.ringowatcher.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;

/**
 * Copyright (C) usalko.com - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ivan Usalko <ivict@usalko.com>, Июнь 2016
 */
public class Other {

    private Other() {}

    public static String asString(String threadName, Throwable th) {
        if (th == null) {
            return "";
        }
        StringWriter writer = new StringWriter();
        writer.append('[').append(new Date().toString()).append(']');
        writer.append('[').append(threadName).append(']').append('\n');
        th.printStackTrace(new PrintWriter(writer));
        return writer.toString();
    }
}
