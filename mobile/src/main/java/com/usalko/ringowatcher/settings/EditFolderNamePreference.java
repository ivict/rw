package com.usalko.ringowatcher.settings;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Parcel;
import android.os.Parcelable;
import android.preference.DialogPreference;
import android.text.InputType;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.ipaulpro.afilechooser.utils.FileUtils;
import com.usalko.ringowatcher.R;
import com.usalko.ringowatcher.RingoWatcherActivity;
import com.usalko.ringowatcher.Settings;
import com.usalko.ringowatcher.util.Res;
import com.usalko.tools.Tagger;

import org.icelog.clog.Clog;

import java.io.File;

/**
 * Copyright (C) usalko.com - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ivan Usalko <ivict@usalko.com>, Май 2016
 */
public class EditFolderNamePreference extends DialogPreference implements Settings.SettingsChangeListener {


    private static final String TAG = Tagger.tag(EditFolderNamePreference.class);
    //Layout Fields
    private final LinearLayout layout = new LinearLayout(this.getContext());
    private final EditText mEditText = new EditText(this.getContext());
    private final ImageButton button = new ImageButton(this.getContext());

    private String mText;

    //Called when addPreferencesFromResource() is called. Initializes basic paramaters
    public EditFolderNamePreference(final Context context, AttributeSet attrs) {
        super(context, attrs);
        setPersistent(true);
        button.setBackgroundColor(Color.TRANSPARENT);
        button.setImageDrawable(new Res(context).getDrawable(R.drawable.ic_chooser_selectable, null));
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showChooser();
            }
        });
        mEditText.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
    }

    private void showChooser() {
        // Use the GET_CONTENT intent from the utility class
        Intent target = FileUtils.createGetContentIntent();
        // Create the chooser Intent
        Intent intent = Intent.createChooser(
                target, getContext().getResources().getString(R.string.folder_chooser_title));
        try {
            ((Activity) getContext()).startActivityForResult(intent, GeneralSettingsActivity.REQUEST_CODE);
        } catch (ActivityNotFoundException e) {
            Clog.e(TAG, e);
        }
    }

    //Create the Dialog view
    @Override
    protected View onCreateDialogView() {
        layout.setOrientation(LinearLayout.HORIZONTAL);
        layout.addView(mEditText);
        layout.addView(button);

        LinearLayout.LayoutParams buttonParams = (LinearLayout.LayoutParams) button.getLayoutParams();
        buttonParams.weight = 0.5f;
        button.setLayoutParams(buttonParams);

        LinearLayout.LayoutParams editTextParams = (LinearLayout.LayoutParams) mEditText.getLayoutParams();
        editTextParams.weight = 1.0f;
        mEditText.setLayoutParams(editTextParams);

        return layout;
    }

    //Attach persisted values to Dialog
    @Override
    protected void onBindDialogView(View view) {
        super.onBindDialogView(view);

        EditText editText = mEditText;
        editText.setText(getText());

//        ViewParent oldParent = editText.getParent();
//        if (oldParent != view) {
//            if (oldParent != null) {
//                ((ViewGroup) oldParent).removeView(editText);
//            }
//            onAddEditTextToDialogView(view, editText);
//        }
        ((RingoWatcherActivity) getContext()).getApplicationContext().getSettings().registerChangeListener(this);
    }

    //persist values and disassemble views
    @Override
    protected void onDialogClosed(boolean positiveresult) {
        super.onDialogClosed(positiveresult);
        if (positiveresult && shouldPersist()) {
            String value = mEditText.getText().toString();
            if (callChangeListener(value)) {
                persistString(value);
                setText(value);
            }
        }

        ((ViewGroup) mEditText.getParent()).removeView(mEditText);
        ((ViewGroup) button.getParent()).removeView(button);
        ((ViewGroup) layout.getParent()).removeView(layout);

        ((RingoWatcherActivity) getContext()).getApplicationContext().getSettings().unregisterChangeListener(this);
        notifyChanged();
    }

    /**
     * Saves the text to the {@link SharedPreferences}.
     *
     * @param text The text to save
     */
    public void setText(String text) {
        final boolean wasBlocking = shouldDisableDependents();

        mText = text;
        mEditText.setText(text);

        persistString(text);

        final boolean isBlocking = shouldDisableDependents();
        if (isBlocking != wasBlocking) {
            notifyDependencyChange(isBlocking);
        }
    }

    @Override
    public void setDefaultValue(Object defaultValue) {
        super.setDefaultValue(defaultValue);
    }

    /**
     * Gets the text from the {@link SharedPreferences}.
     *
     * @return The current preference value.
     */
    public String getText() {
        return mText;
    }

    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        return a.getString(index);
    }

    @Override
    protected void onSetInitialValue(boolean restoreValue, Object defaultValue) {
        setText(restoreValue ? getPersistedString(mText) : (String) defaultValue);
    }

    @Override
    public boolean shouldDisableDependents() {
        return TextUtils.isEmpty(mText) || super.shouldDisableDependents();
    }

    /**
     * Returns the {@link EditText} widget that will be shown in the dialog.
     *
     * @return The {@link EditText} widget that will be shown in the dialog.
     */
    public EditText getEditText() {
        return mEditText;
    }

//    /**
//     * Adds the EditText widget of this preference to the dialog's view.
//     *
//     * @param dialogView The dialog view.
//     */
//    protected void onAddEditTextToDialogView(View dialogView, EditText editText) {
//        ViewGroup container = (ViewGroup) dialogView
//                .findViewById(com.android.internal.R.id.edittext_container);
//        if (container != null) {
//            container.addView(editText, ViewGroup.LayoutParams.MATCH_PARENT,
//                    ViewGroup.LayoutParams.WRAP_CONTENT);
//        }
//    }

    @Override
    protected Parcelable onSaveInstanceState() {
        final Parcelable superState = super.onSaveInstanceState();
        if (isPersistent()) {
            // No need to save instance state since it's persistent
            return superState;
        }

        final SavedState myState = new SavedState(superState);
        myState.text = getText();
        return myState;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        if (state == null || !state.getClass().equals(SavedState.class)) {
            // Didn't save state for us in onSaveInstanceState
            super.onRestoreInstanceState(state);
            return;
        }

        SavedState myState = (SavedState) state;
        super.onRestoreInstanceState(myState.getSuperState());
        setText(myState.text);
    }

    @Override
    public void onChangeSetting(String key, Object value) {
        if (Settings.FOLDER_FOR_AUTOPLAY.equals(key)) {
            setText(value != null ? ((File) value).getPath() : "");
        }
    }

    private static class SavedState extends BaseSavedState {
        String text;

        public SavedState(Parcel source) {
            super(source);
            text = source.readString();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            super.writeToParcel(dest, flags);
            dest.writeString(text);
        }

        public SavedState(Parcelable superState) {
            super(superState);
        }

        public static final Parcelable.Creator<SavedState> CREATOR =
                new Parcelable.Creator<SavedState>() {
                    public SavedState createFromParcel(Parcel in) {
                        return new SavedState(in);
                    }

                    public SavedState[] newArray(int size) {
                        return new SavedState[size];
                    }
                };
    }
}