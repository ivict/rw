package com.usalko.ringowatcher.settings;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;

import com.ipaulpro.afilechooser.utils.FileUtils;
import com.usalko.ringowatcher.RingoWatcherActivity;
import com.usalko.ringowatcher.fragments.GeneralPreferenceFragment;
import com.usalko.tools.Tagger;

import org.icelog.clog.Clog;

import java.io.File;

/**
 * Copyright (C) usalko.com - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ivan Usalko <ivict@usalko.com>, Май 2016
 */
public class GeneralSettingsActivity extends RingoWatcherActivity {

    private static final String TAG = Tagger.tag(GeneralSettingsActivity.class);

    public static final int REQUEST_CODE = 6384; // onActivityResult request

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Display the fragment as the main content.
        getFragmentManager().beginTransaction().replace(android.R.id.content,
                new GeneralPreferenceFragment()).commit();

        getApplicationContext().getSettings().increaseCountOfSettingsShow();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Clog.d(TAG, "on-options-item-selected");
        int id = item.getItemId();
        if (id == android.R.id.home) {
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CODE:
                // If the file selection was successful
                if (resultCode == RESULT_OK) {
                    if (data != null) {
                        // Get the URI of the selected file
                        final Uri uri = data.getData();
                        Clog.i(TAG, "Uri = " + uri.toString());
                        try {
                            // Get the file path from the URI
                            final String path = FileUtils.getPath(this, uri);
                            getApplicationContext().getSettings().setFolderForAutoplay(getFolder(path));
                        } catch (Exception e) {
                            Clog.e(TAG, "File select error", e);
                        }
                    }
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @NonNull
    private File getFolder(String path) {
        File file = new File(path);
        if (!file.isDirectory()) {
            return file.getParentFile();
        }
        return file;
    }

}
