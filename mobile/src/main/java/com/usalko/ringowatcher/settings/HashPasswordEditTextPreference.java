package com.usalko.ringowatcher.settings;

import android.content.Context;
import android.preference.EditTextPreference;
import android.util.AttributeSet;

import com.usalko.ringowatcher.util.Hash;

/**
 * Copyright (C) usalko.com - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ivan Usalko <ivict@usalko.com>, Май 2016
 */
public class HashPasswordEditTextPreference extends EditTextPreference {

    private boolean accepted;

    public HashPasswordEditTextPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void setText(String text) {
        if (accepted) {
            super.setText(Hash.sha256(text));
        }
    }

    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }
}
