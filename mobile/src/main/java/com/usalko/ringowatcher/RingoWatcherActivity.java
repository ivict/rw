package com.usalko.ringowatcher;

import android.support.v7.app.AppCompatActivity;

/**
 * Copyright (C) usalko.com - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ivan Usalko <ivict@usalko.com>, Май 2016
 */
abstract public class RingoWatcherActivity  extends AppCompatActivity {

    @Override
    public GlobalContext getApplicationContext() {
        return (GlobalContext) super.getApplicationContext();
    }
}
