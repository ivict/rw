package com.usalko.ringowatcher.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.usalko.ringowatcher.RingoService;
import com.usalko.tools.Tagger;

import org.icelog.clog.Clog;

/**
 * Copyright (C) usalko.com - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ivan Usalko <ivict@usalko.com>, Май 2016
 */
public class StartRingoServiceAtBootReceiver extends BroadcastReceiver {

    private static final String TAG = Tagger.tag(StartRingoServiceAtBootReceiver.class);

    @Override
    public void onReceive(Context context, Intent intent) {
        Clog.d(TAG, "on-receive:" + intent);
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            Intent serviceIntent = new Intent(context, RingoService.class);
            serviceIntent.setAction(Intent.ACTION_BOOT_COMPLETED);
            context.startService(serviceIntent);
        }
    }
}