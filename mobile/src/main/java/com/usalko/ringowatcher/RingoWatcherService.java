package com.usalko.ringowatcher;

import android.app.Service;

/**
 * Copyright (C) usalko.com - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ivan Usalko <ivict@usalko.com>, Май 2016
 */
public abstract class RingoWatcherService extends Service {

    @Override
    public GlobalContext getApplicationContext() {
        return (GlobalContext) super.getApplicationContext();
    }
}
