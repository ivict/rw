package com.usalko.ringowatcher;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.FileObserver;
import android.support.annotation.NonNull;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.WindowManager;
//import android.widget.TextView;
import android.widget.VideoView;

import com.usalko.ringowatcher.settings.GeneralSettingsActivity;
import com.usalko.tools.Tagger;

//import org.icelog.clog.Clog;
//import org.icelog.clog.buffer.LBufferDrv;

import org.icelog.clog.Clog;

import java.io.File;
import java.io.FileFilter;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class MainActivity extends RingoWatcherActivity implements Settings.SettingsChangeListener
        , LoaderManager.LoaderCallbacks<List<File>> {

    private static final int SHOW_PREFERENCES = 1;
    /**
     * Whether or not the system UI should be auto-hidden after
     * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private static final boolean AUTO_HIDE = true;

    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
     * user interaction before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

    /**
     * Some older devices needs a small delay between UI widget updates
     * and a change of the status and navigation bar.
     */
    private static final int UI_ANIMATION_DELAY = 300;
    private static final String TAG = Tagger.tag(MainActivity.class);
    private final Handler mMainHandler = new Handler();
    private VideoView mContentView;
    private final Runnable mHidePart2Runnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            // Delayed removal of status and navigation bar

            // Note that some of these constants are new as of API 16 (Jelly Bean)
            // and API 19 (KitKat). It is safe to use them, as they are inlined
            // at compile-time and do nothing on earlier devices.
            mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    //| View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    };
    //private View mControlsView;
    private final Runnable mShowPart2Runnable = new Runnable() {
        @Override
        public void run() {
            // Delayed display of UI elements
            ActionBar actionBar = getSupportActionBar();
            if (actionBar == null) {
                Clog.d(TAG, "Action bar is null");
                return;
            }
            //actionBar.show();
            //mControlsView.setVisibility(View.VISIBLE);
        }
    };
    private boolean mVisible;
    private final Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            hide();
        }
    };
//    /**
//     * Touch listener to use for in-layout UI controls to delay hiding the
//     * system UI. This is to prevent the jarring behavior of controls going away
//     * while interacting with activity UI.
//     */
//    private final View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
//        @Override
//        public boolean onTouch(View view, MotionEvent motionEvent) {
//            if (AUTO_HIDE) {
//                delayedHide(AUTO_HIDE_DELAY_MILLIS);
//            }
//            return false;
//        }
//    };
    private final Runnable mPeekFolderObserverRunnable = new Runnable() {
        @Override
        public void run() {
            peekFolderObserver();
        }
    };
    private final Runnable mPeekNextPlayWatcherRunnable = new Runnable() {
        @Override
        public void run() {
            peekNextPlayWatcher();
        }
    };

    //String path;
    //private MediaPlayer mp;
    //private SurfaceHolder holder;
    //boolean pausing = false;
    List<File> playList = new ArrayList<>();
    int playListPosition = 0;
    private Uri videoURI;
    private FileObserver observer;
    private MediaPlayer.OnPreparedListener onPreparedListener;
    private boolean activityPaused;
    private File prevFolder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        //Wake lock screen
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        mVisible = true;
        //mControlsView = findViewById(R.id.fullscreen_content_controls);
        mContentView = (VideoView) findViewById(R.id.fullscreen_content);

        // Set up the user interaction to manually show or hide the system UI.
        mContentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggle();
            }
        });

        // Upon interacting with UI controls, delay any scheduled hide()
        // operations to prevent the jarring behavior of controls going away
        // while interacting with the UI.
        //findViewById(R.id.dummy_button).setOnTouchListener(mDelayHideTouchListener);

        mContentView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mContentView.post(new Runnable() {
                    @Override
                    public void run() {
                        nextPlay();
                    }
                });
            }
        });
        mContentView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                //COMMENT-FOR-DEBUG>
                mContentView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        nextPlay();
                    }
                }, TimeUnit.SECONDS.toMillis(Settings.MIN_TIME_PLAY_NEXT_AFTER_ERROR_IN_SECONDS));
                return true;
                //throw new IllegalStateException("Media-player-error: " + what + ", extra:" + extra);
            }
        });
        getApplicationContext().setMainActivityLaunched(true);
        getSupportLoaderManager().initLoader(0, null, this);
        fixShowMenuOnSamsungDevices();
    }

//    private void logToTextView() {
//        final int limit = 2048;
//        final TextView textView = (TextView) findViewById(R.id.activity_main_$debug$_text_view);
//        LBufferDrv.setBuffer(new ClogBuffer() {
//
//            @Override
//            public void writeText(String text) {
//                System.out.println("###" + text);
//                if (TextUtils.isEmpty(text) || textView == null) {
//                    System.out.println("############### textView is null or text is empty ###########");
//                    return;
//                }
//                final SpannableStringBuilder outText = new SpannableStringBuilder();
//                CharSequence textViewText = textView.getText();
//                int l1 = textViewText.length();
//                int l2 = text.length();
//                if (l1 + l2 < limit) {
//                    outText.append(textViewText).append(text);
//                } else {
//                    int remove = l1 - (limit - Math.min(l2, limit));
//                    if (remove < l1) {
//                        outText.append(textViewText.subSequence(remove, l1)).append(text);
//                    } else {
//                        outText.append(text.subSequence(0, Math.min(l2, limit)));
//                    }
//                }
//                System.out.println("### TRY TO TEXT OUT 1###");
//                mMainHandler.post(new Runnable() {
//                    @Override
//                    public void run() {
//                        System.out.println("### TRY TO TEXT OUT 2###");
//                        textView.setText(outText);
//                    }
//                });
//
//            }
//        });
//    }

    //http://stackoverflow.com/questions/26870274/actionbar-setting-button-not-appear-in-actionbar-on-samsung-device
    private void fixShowMenuOnSamsungDevices() {
        try {
            ViewConfiguration config = ViewConfiguration.get(this);
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
            if (menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (Exception ex) {
            // Ignore
        }
    }

    private List<File> refreshPlayList() {
        File folder = getApplicationContext().getSettings().getFolderForAutoplay();
        Clog.v(TAG, "refresh-play-list:folder:" + folder);
        List<File> list = new ArrayList<File>();
        updatePlayListForFolder(folder, list);
        return list;
    }

    private void updatePlayListForFolder(File folder, final List<File> list) {
        if (folder == null) {
            Clog.e(TAG, "File folder is null");
            return;
        }
        if (!folder.exists()) {
            Clog.e(TAG, "File folder not exists");
            return;
        }
        if (!folder.isDirectory()) {
            Clog.e(TAG, "File folder not directory");
            return;
        }
        list.addAll(Arrays.asList(folder.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                String name = pathname.getName();
                if (!isAcceptedVideoFormat(pathname)) {
                    return false;
                }
                if (name.startsWith(".")) {//Hack for MXQ Pro
                    return false;
                }
                Clog.v(TAG, "add-play-list-item: " + pathname);
                return true;
            }
        })));
    }

    private boolean isAcceptedVideoFormat(File videoFile) {
        if (videoFile == null) {
            Clog.i(TAG, "is-accepted-video-format[videoFile is null]");
            return false;
        }
        //TODO: CHECK CORRECT VIDEO FORMATS
        String name = videoFile.getName();
        if (name.endsWith(".flv")) {
            //TODO: possible listen audio with animation
            return false;
        }
        if (name.endsWith(".jpg")) {
            return false;
        }
        if (name.endsWith(".jpeg")) {
            return false;
        }
        if (name.endsWith(".gif")) {
            return false;
        }
        if (name.endsWith(".xls")) {
            return false;
        }
        if (name.endsWith(".doc")) {
            return false;
        }
        if (name.endsWith(".mp3")) {
            return false;
        }
        return true;
    }

    private void nextPlay() {
        if (playList == null || playList.isEmpty()) {
            Clog.e(TAG, "Play list is empty");
            if (mContentView.canPause()) {
                mContentView.pause();
                mContentView.stopPlayback();
            }
            return;
        }
        //Check if player play file, then wait for stop play before play next
        if (mContentView.isPlaying()) {
            Clog.d(TAG, "disable-next-play [because playing file: " + videoURI + "]");
            return;
        }
        correctPlayListPosition();
        try {
            File videoFile = playList.get(playListPosition++);
            if (!videoFile.exists()) {
                mContentView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        nextPlay();
                    }
                }, TimeUnit.SECONDS.toMillis(Settings.MIN_TIME_PLAY_NEXT_AFTER_ERROR_IN_SECONDS));
            }
            Clog.d(TAG, "play-next:" + videoFile);
            videoURI = Uri.fromFile(videoFile);

            mContentView.start();
            mContentView.setVideoURI(videoURI);
            mContentView.setOnPreparedListener(getOnPreparedListener());
        } catch (Throwable th) {
            //COMMENT-FOR-DEBUG>
            Clog.e(TAG, th);
            //throw new RuntimeException(th);
        }
    }

    @NonNull
    private MediaPlayer.OnPreparedListener getOnPreparedListener() {
        if (onPreparedListener != null) {
            return onPreparedListener;
        }
        synchronized (this) {
            if (onPreparedListener != null) {
                return onPreparedListener;
            }
            return new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mContentView.start();
                }
            };
        }
    }

    private void stopPlay() {
        Clog.d(TAG, "stop-play:" + playListPosition);
        mContentView.stopPlayback();
    }

    private void correctPlayListPosition() {
        //Correct wrong values
        if (playListPosition >= playList.size()
                || playListPosition < 0) {
            playListPosition = 0;
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        delayedHide(100);
    }

    private void toggle() {
        if (mVisible) {
            hide();
        } else {
            show();
        }
    }

    private void hide() {
        // Hide UI first
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        //mControlsView.setVisibility(View.GONE);
        mVisible = false;

        // Schedule a runnable to remove the status and navigation bar after a delay
        mMainHandler.removeCallbacks(mShowPart2Runnable);
        mMainHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY);
    }

    @SuppressLint("InlinedApi")
    private void show() {
        // Show the system bar
        mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        mVisible = true;

        // Schedule a runnable to display UI elements after a delay
        mMainHandler.removeCallbacks(mHidePart2Runnable);
        mMainHandler.postDelayed(mShowPart2Runnable, UI_ANIMATION_DELAY);
    }

    /**
     * Schedules a call to hide() in [delay] milliseconds, canceling any
     * previously scheduled calls.
     */
    private void delayedHide(int delayMillis) {
        mMainHandler.removeCallbacks(mHideRunnable);
        mMainHandler.postDelayed(mHideRunnable, delayMillis);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        Clog.d(TAG, "on-new-intent");
        super.onNewIntent(intent);
        peekFolderObserver();
    }

    @Override
    protected void onDestroy() {
        Clog.d(TAG, "on-destroy");
        super.onDestroy();
        getApplicationContext().setMainActivityLaunched(false);
    }

    @Override
    protected void onStart() {
        Clog.d(TAG, "on-start");
        super.onStart();
        if (isFirstAppRunAfterInstallAndNoOnceSettingsShowOrWrongFolder()) {
            showSettings();
        }
        getApplicationContext().getSettings().registerChangeListener(this);
    }

    private AlertDialog showLastError(String error) {
        return new AlertDialog.Builder(this)
                .setTitle("Last Error")
                .setMessage(error)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // clear error
                        getApplicationContext().getSettings().setLastError("");
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopFolderObserver();
        getApplicationContext().getSettings().setPlayListPosition(playListPosition);
        onPreparedListener = null;
        System.out.println("### CLEAR BUFFER");
        //LBufferDrv.setBuffer(null);
    }

    @Override
    protected void onResume() {
        super.onResume();
        playListPosition = getApplicationContext().getSettings().getPlayListPosition();
        if (!isFirstAppRunAfterInstallAndNoOnceSettingsShowOrWrongFolder()) {
            //logToTextView();
            startFolderObserver();
        }
    }

    private void showSettings() {
        Intent i = new Intent(this, GeneralSettingsActivity.class);
        startActivityForResult(i, SHOW_PREFERENCES);
    }

    private boolean isFirstAppRunAfterInstallAndNoOnceSettingsShowOrWrongFolder() {
        if (!getApplicationContext().getSettings().isRegistered()) {
            return true;
        }
        if (getApplicationContext().getSettings().getCountOfAppRunAfterInstall() == 1
                && getApplicationContext().getSettings().getCountOfSettingsShow() == 0) {
            return true;
        }
        //if (!getApplicationContext().getSettings().getFolderForAutoplay().exists()) {
        //    return true;
        //}
        return false;
    }

    @Override
    protected void onStop() {
        Clog.d(TAG, "on-stop");
        stopPlay();
        getApplicationContext().getSettings().unregisterChangeListener(this);
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.main_menu_item_settings:
                showSettings();
                return true;
            case R.id.main_menu_item_last_error:
                String lastError = getApplicationContext().getSettings().getLastError();
                if (!TextUtils.isEmpty(lastError)) {
                    showLastError(lastError);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onChangeSetting(String key, Object value) {
        if (Settings.FOLDER_FOR_AUTOPLAY.equals(key)) {
            peekFolderObserver();
        }
    }

    @Override
    public Loader<List<File>> onCreateLoader(int id, Bundle args) {
        return new AsyncTaskLoader<List<File>>(this) {

            @Override
            public List<File> loadInBackground() {
                return refreshPlayList();
            }
        };
    }

    @Override
    public void onLoadFinished(Loader<List<File>> loader, List<File> data) {
        if (data == null) {
            return;
        }
        //check real event
        Set<File> set1 = new HashSet<File>(playList);
        Set<File> set2 = new HashSet<File>(data);
        if (set1.equals(set2)) {
            return;
        }
        //Yep real changes of file list
        playList.clear();
        playList.addAll(data);
        Collections.sort(playList);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //Reset play list positions after play
                playListPosition = 0;
                nextPlay();
            }
        });
    }

    private void startFolderObserver() {
        activityPaused = false;
        getSupportLoaderManager().getLoader(0).forceLoad();
        peekFolderObserver();
        peekNextPlayWatcher();
    }

    private void peekNextPlayWatcher() {
        if (activityPaused) {
            Clog.d(TAG, "peek-next-play-watcher[finish because activityPaused = true]");
            return;
        }
        if (mContentView != null && !mContentView.isPlaying()) {
            nextPlay();
        }
        mMainHandler.removeCallbacks(mPeekNextPlayWatcherRunnable);
        mMainHandler.postDelayed(mPeekNextPlayWatcherRunnable
                , TimeUnit.SECONDS.toMillis(2 * Settings.MIN_TIME_PLAY_NEXT_AFTER_ERROR_IN_SECONDS));
    }

    private void peekFolderObserver() {
        if (activityPaused) {
            Clog.d(TAG, "peek-folder-observer[finish because activityPaused = true]");
            return;
        }
        final File folder = getApplicationContext().getSettings().getFolderForAutoplay();
        //If folder exists start observer
        if (folder.exists()) {
            getSupportLoaderManager().getLoader(0).onContentChanged();
            initAndStartObserverIfNotInited(folder);
        } else {
            mMainHandler.removeCallbacks(mPeekFolderObserverRunnable);
            mMainHandler.postDelayed(mPeekFolderObserverRunnable
                    , TimeUnit.SECONDS.toMillis(Settings.MIN_CHECK_FOLDER_EXISTENS_INTERVAL_IN_SECONDS));
        }
    }

    private void initAndStartObserverIfNotInited(final File folder) {
        if (!folder.equals(prevFolder)) {
            Clog.d(TAG, "folder-changed restart observer");
            if (observer != null) {
                observer.stopWatching();
            }
            observer = null;
            prevFolder = folder;
        }
        if (observer != null) {
            return;
        }
        Clog.d(TAG, "start-observer [for folder: " + folder + "]");
        observer = new FileObserver(folder.getAbsolutePath()
                , FileObserver.CREATE
                | FileObserver.DELETE
                | FileObserver.DELETE_SELF
                | FileObserver.MOVE_SELF
                | FileObserver.MOVED_FROM
                | FileObserver.MOVED_TO) {
            @Override
            public void onEvent(int event, String path) {
                Clog.d(TAG, "event [" + event + ", " + path + "] for folder: " + folder);
                getSupportLoaderManager().getLoader(0).onContentChanged();
            }
        };
        observer.startWatching();
    }

    private void stopFolderObserver() {
        activityPaused = true;
        if (observer == null) {
            return;
        }
        final File folder = getApplicationContext().getSettings().getFolderForAutoplay();
        Clog.d(TAG, "stop-observer [for folder: " + folder + "]");
        observer.stopWatching();
    }

    @Override
    public void onLoaderReset(Loader<List<File>> loader) {
        Clog.d(TAG, "on-loader-reset");
    }
}
