package com.usalko.ringowatcher;

/**
 * Copyright (C) usalko.com - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ivan Usalko <ivict@usalko.com>, Май 2016
 */
public class RwIntent {

    private RwIntent() { }

    public static String REREAD_LIST_ACTION = "com.usalko.ringowatcher.action.REREAD_LIST_ACTION";

}
