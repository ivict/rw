package com.usalko.ringowatcher;

import java.io.File;

/**
 * Copyright (C) usalko.com - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ivan Usalko <ivict@usalko.com>, Май 2016
 */
public interface Settings {

    boolean DEFAULT_ENABLE_AUTOPLAY_ON_REBOOT = true;

    String FOLDER_FOR_AUTOPLAY = "folder_for_autoplay";
    String ENABLE_AUTOPLAY_ON_REBOOT = "enable_autoplay_on_reboot";
    String PASSWORD_HASH = "password_hash";
    String PLAY_LIST_POSITION = "play_list_position";
    int DEFAULT_PLAY_LIST_POSITION = 0;
    int MIN_CHECK_FOLDER_EXISTENS_INTERVAL_IN_SECONDS = 3;
    int MIN_TIME_PLAY_NEXT_AFTER_ERROR_IN_SECONDS = 5;
    String LAST_ERROR = "last_error";

    File getFolderForAutoplay();

    void setFolderForAutoplay(File folderForAutoplay);

    boolean isEnableAutoplayOnReboot();

    void setEnableAutoplayOnReboot(boolean enableAutoplayOnReboot);

    int getCountOfAppRunAfterInstall();

    int getCountOfSettingsShow();

    void increaseCountOfSettingsShow();

    void registerChangeListener(SettingsChangeListener settingsChangeListener);

    void unregisterChangeListener(SettingsChangeListener settingsChangeListener);

    String getDefaultFolderForAutoplay();

    boolean isRegistered();

    void setPasswordHash(String passwordHash);

    void setPlayListPosition(int playListPosition);

    int getPlayListPosition();

    String getLastError();

    void setLastError(String lastError);

    interface SettingsChangeListener {
        void onChangeSetting(String key, Object value);
    }
}
