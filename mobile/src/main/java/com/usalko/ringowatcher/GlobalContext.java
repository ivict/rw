package com.usalko.ringowatcher;

import android.app.Application;

import com.usalko.ringowatcher.receiver.Alarm;
import com.usalko.ringowatcher.util.Other;
import com.usalko.tools.Tagger;

import org.icelog.clog.Clog;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Copyright (C) usalko.com - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ivan Usalko <ivict@usalko.com>, Май 2016
 */
public class GlobalContext extends Application {
    private static final String TAG = Tagger.tag(GlobalContext.class);

    static {
        Tagger.setPrefix("com.usalko.rw.");
        //FOR-DEBUG>
        Clog.setLevel(Clog.DEBUG);
        //Clog.setIgnoreIsLoggable(true);
    }

    private Alarm alarm = new Alarm();
    private AtomicBoolean mainActivityLaunched = new AtomicBoolean();
    private Settings settings;

    @Override
    public void onCreate() {
        super.onCreate();
        Thread.currentThread().setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable ex) {
                //TODO: send error
                Clog.e(TAG + "[" + thread.getName() + "]", ex);
                getSettings().setLastError(Other.asString(thread.getName(), ex));
                //UNCOMMENT-FOR-DEBUG>
                //throw new IllegalStateException(ex);
            }
        });
        Clog.i(TAG, "on-create:set-alarm");
        alarm.cancelAlarm(this);
        //COMMENT-BELOW-LINE-FOR-DEBUG>
        alarm.setAlarm(this);
        settings = new SettingsStorageImpl(this);
    }

    public boolean isMainActivityLaunched() {
        return mainActivityLaunched.get();
    }

    public void setMainActivityLaunched(boolean mainActivityLaunched) {
        this.mainActivityLaunched.set(mainActivityLaunched);
    }

    public Settings getSettings() {
        return settings;
    }


}
