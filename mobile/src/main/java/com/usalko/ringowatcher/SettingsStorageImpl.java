package com.usalko.ringowatcher;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.preference.PreferenceManager;

import com.usalko.ringowatcher.util.Hash;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) usalko.com - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ivan Usalko <ivict@usalko.com>, Май 2016
 */
class SettingsStorageImpl implements Settings {

    private String DEFAULT_FOLDER_FOR_AUTOPLAY = new File(Environment.getExternalStorageDirectory(), "Movies").getPath();

    private final Context context;
    private final List<SettingsChangeListener> listeners;

    public SettingsStorageImpl(Context context) {
        this.context = context;
        //Update run counter
        increaseCountOfAppRunAfterInstall();
        listeners = new ArrayList<>();
    }

    private void increaseCountOfAppRunAfterInstall() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        int countOfRun = prefs.getInt("count_of_app_run_after_install", 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt("count_of_app_run_after_install", countOfRun + 1);
        editor.apply();
    }

    @Override
    public File getFolderForAutoplay() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String folder = prefs.getString(FOLDER_FOR_AUTOPLAY, getDefaultFolderForAutoplay());
        return new File(folder);
    }

    @Override
    public void setFolderForAutoplay(File folderForAutoplay) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Settings.FOLDER_FOR_AUTOPLAY, folderForAutoplay.getPath());
        editor.apply();
        fireChangeSettings(Settings.FOLDER_FOR_AUTOPLAY, folderForAutoplay);
    }

    @Override
    public boolean isEnableAutoplayOnReboot() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getBoolean(ENABLE_AUTOPLAY_ON_REBOOT, DEFAULT_ENABLE_AUTOPLAY_ON_REBOOT);
    }

    @Override
    public void setEnableAutoplayOnReboot(boolean enableAutoplayOnReboot) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(Settings.ENABLE_AUTOPLAY_ON_REBOOT, enableAutoplayOnReboot);
        editor.apply();
        fireChangeSettings(Settings.ENABLE_AUTOPLAY_ON_REBOOT, enableAutoplayOnReboot);
    }

    @Override
    public int getCountOfAppRunAfterInstall() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getInt("count_of_app_run_after_install", 1);
    }

    @Override
    public int getCountOfSettingsShow() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getInt("count_of_settings_show", 0);
    }

    @Override
    public void increaseCountOfSettingsShow() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        int countOfShow = prefs.getInt("count_of_settings_show", 0);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt("count_of_settings_show", countOfShow + 1);
        editor.apply();
    }

    @Override
    public void registerChangeListener(SettingsChangeListener settingsChangeListener) {
        listeners.add(settingsChangeListener);
    }

    @Override
    public void unregisterChangeListener(SettingsChangeListener settingsChangeListener) {
        listeners.remove(settingsChangeListener);
    }

    @Override
    public String getDefaultFolderForAutoplay() {
        File externalFolder = findVideoFolder();
        if (externalFolder == null) {
            return DEFAULT_FOLDER_FOR_AUTOPLAY;
        }
        return externalFolder.getPath();
    }

    private File findVideoFolder() {
        //TODO: find video folder
        return null;
    }

    protected void fireChangeSettings(String key, Object value) {
        for (SettingsChangeListener listener : listeners) {
            listener.onChangeSetting(key, value);
        }
    }

    @Override
    public boolean isRegistered() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String passwordHash = prefs.getString(PASSWORD_HASH, "");
        return passwordHash.equals(Hash.DEFAULT_PASSWORD_HASH);
    }

    @Override
    public void setPasswordHash(String passwordHash) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Settings.PASSWORD_HASH, passwordHash);
        editor.apply();
        fireChangeSettings(Settings.PASSWORD_HASH, passwordHash);
    }

    @Override
    public void setPlayListPosition(int playListPosition) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(Settings.PLAY_LIST_POSITION, playListPosition);
        editor.apply();
        fireChangeSettings(Settings.PLAY_LIST_POSITION, playListPosition);
    }

    @Override
    public int getPlayListPosition() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getInt(PLAY_LIST_POSITION, DEFAULT_PLAY_LIST_POSITION);
    }

    @Override
    public String getLastError() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString(LAST_ERROR, "");
    }

    @Override
    public void setLastError(String lastError) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Settings.LAST_ERROR, lastError);
        editor.apply();
        fireChangeSettings(Settings.LAST_ERROR, lastError);
    }
}
