package com.usalko.ringowatcher;

import android.content.Intent;
import android.os.IBinder;

import com.usalko.tools.Tagger;

import org.icelog.clog.Clog;

import java.util.Date;

public class RingoService extends RingoWatcherService {
    private static final String TAG = Tagger.tag(RingoService.class);

    public RingoService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        Clog.i(TAG, "on-bind");
        return null;
    }

    @Override
    public void onDestroy() {
        Clog.i(TAG, "on-destroy");
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Clog.d(TAG, "on-start-command: " + intent);
        //Service can be start only on autoboot and no need to restart.
        //http://stackoverflow.com/questions/5856861/why-android-service-crashes-with-nullpointerexception
        if (intent == null
                || getApplicationContext().isMainActivityLaunched()
                || !getApplicationContext().getSettings().isEnableAutoplayOnReboot()) {
            return 0;
        }
        Clog.i(TAG, "start-main-activity[" + new Date() + "]");
        Intent startWatch = new Intent(getBaseContext(), MainActivity.class);
        startWatch.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startWatch.setAction(intent.getAction());
        startActivity(startWatch);
        return 0;
    }

}
