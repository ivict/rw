package com.usalko.tools;

/**
 * Copyright (C) usalko.com - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ivan Usalko <ivict@usalko.com>, Май 2016
 */
public class Tagger {

    private static String prefix = "";

    private Tagger() {}


    public static String tag(Class<?> c) {
        return prefix + c.getSimpleName();
    }

    public static void setPrefix(String prefix) {
        Tagger.prefix = prefix;
    }
}
